<img src=/assets/banner.png>

welcome! these are my <b>personal</b> experimentation of alternative gnome shell light theme. because currently there is no official discussion from gnome dev.

## Current Status

<img width="300" src=/assets/teaser.gif>

latest development are moved here: https://gitlab.com/dikasetyaprayogi/light-shell

the holistic (full) gnome shell light theme is still in discussion. there still no official clarification from gnome project. however some devs are eager to take on the idea, they just demand to refine the proposed tweaks and testing it further.

originally im planning to create ready to use gnome shell light theme build but currently im busy with my life, in the meantime you can build the raw and more updated version from Elliot Shuggerman git (see credits at bottom page) on which these project based on.

the community gnome shell light theme port itself is ready. there just need some papercuts to fix but is usable. there is continous development to improve the brighness and contrast to be comfortable for daily use.

## About this Project

this project is whiteboards. if you want to issue code fixes, please do so in original projects that makes these light shell possible (credits at bottom page).

what i proposed here is not to replace current gnome theming, rather than to give user choice and fullfiling user needs.

- video demo [here](https://youtu.be/VjWcQbahq9Y)
- (temporarily archived, please use theme provided by Elliot Shuggerman instead, credits at bootm page) download theme [here](https://gitlab.gnome.org/dikasp2/gnome-shell-light-theme-demo/-/blob/main/gnome-shell-light-theme-demo.zip)
- view main discussion [here](https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/5545)
- view fedora discussion [here](https://discussion.fedoraproject.org/t/light-shell-for-gnome-desktop-experimentation/79656)
- labs (extra artwork & images) [here](https://gitlab.gnome.org/dikasp2/gnome-shell-light-theme-labs)

thanks for your feedback and support. this proposal is stable now, expect less frequent changes on this documentation.



#### PAPERS

## the Issue with Current GNOME Shell Light Theme

|default (partial light theme)|proposed (full light theme)|
|:-------------------------:|:-------------------------:|
|<img width="300" src="assets/img1.png">|<img width="300" src="assets/img1b.png">|
|<img width="300" src="assets/img2.png">|<img width="300" src="assets/img2b.png">|

theme is subjective matter. be it dark or light, they had their own advantage and disadvantage. the important thing is to strike the balance and fullfill the general needs of user.

because gnome discourage theming i hope there at least for the least a light and dark workspace choice for user. not just one general dark workspace for light and dark theme.

with current consensus, the result is a partial light theme (the default theme) that we light user think it doesnt fit best (inconsistent) and doesnt confrom of what light theme purpose eg: to be used with colorful backgrounds, to be used in light workplace, to be used in light bezel laptop/monitor (yes they still exist), etc..

<img width="300" src="/assets/Untitled.png">

## Challenges

there is actually a upstream light gnome shell theme used in gnome shell classic session that can be used as port. but gnome classic is mainly a tweaks and developed separately, thus the light colorscheme not fully compatible with default gnome interface.

- because the current design doesnt put awareness on full light theme, we cant simply inverted the default colorscheme into light. example: the pure black top bar will be too bright if its inverted to white, therefore its needs to tone down into gray a bit.
- some elements not embedded in stylesheet, this need manual fix. example: appearance of high contrast icons
- to bring the full light theme implementation, it is best to ask main gnome project to maintains and incorporated this proposal into gnome HIG.

## Possible Drawbacks

- since the light theme proposed is just an addition fixes, previous accustomed user can continue use dark gnome shell variant with default/mixed apps configuration.
- a slight change in design paradigm, full light theme will take as additional consideration during design.

## Benefits

<img width="300" src="assets/A.png">

### general

- it's more logical. imagine the confusion when user presented with "default and global dark theme" in settings. i belive the most user expectation are: these should be called "light and dark" and when i turn the light theme on, everything should be light as well, not just the applications.
- simplify the design. the desktop shouldn't tell the theme for apps (mixed & global dark), it's just the apps that should tell their themes to the desktop (follow system/default light/default dark)
- conformity with other mainstream light and dark theme concept (android, windows, mac)

### personal

- light theme is more colorful, clear, and intuitive as an alternative.
- unleash the full light theme potential to make the desktop gorgeous and enjoyable to use.
- community can use light shell when making light usecase extension, not hacking and converting dark gnome shell all the time.

## Usability Concerns

### it gets in user way

<img width="300" src="assets/D.png">

see the bear overthere ? you might get distracted by the colorful background and presence of white interface. but you also fell a sense of logical space, like some tv floating in your background world.

so afterall and my personal experience, they are finely distracting in non disturbing manner. moreover its looks appealing with your background fell. anyway when user need an absolut focus we always had the "full screen interface" technology.

### health concerns

<img width="300" src=/assets/fedoralight.gif>
<img width="300" src=/assets/fedoralight.png>

light theming doesn't mean everything should be bright. above are an example of approriate light composition. despite its mostly light colored it still fells comfortable. 

i had consult with my friend who is a doctor asking wheter light theme is bad and he says thats not necessarily true, the one that bad for your health is using unapproriate light condition.

think it like a dark theme user in bright room or light theme user in dark room both had equal eyestrain problems. the more deviation between your display and lightness in your environment the more pressure you put into your eyes adapting to these light conditions. thats why we have brighness slider to adjust the amount of lightning your display used regardless what theme you are using.

### blurs

<img width="300" src=/assets/myeyes.png>

some user suggest that light theme goes best with blurs. this is true however it have its own drawback: the content will be harder to grasp equal to the amount of blur. this will be not preferable by eldery and simplicity advocate, thus it is best to stick to general user needs. the blur my shell extension is available if you need this.

### modern design trends

modern design trends suggest usability is related with color usecase. some developers takes this as to ban light color whenever approriate. you see there is change of dark only media player and image viewer, our good'ol bootsplash are replaced with black screen, the rise of black bezel and notch on our every device, and why latest pixel phone had black only notification QS panel, etc..

as the long time consumer i fell that this concept is wrong. what needs to be right is to balance the color itself to be comfortable to use (not too dark to grasp or not too bright to see), not to discriminate color.

more case of designer and consumer differentation on the take of light theming:
- the case of windows 10 (originally the interface elements are black, after some user feedback later microsoft decided to make windows 10 light theme).
- the case of android 12 (some system element replaced by black only color. most custom rom and OEMs doesnt embrace google ideas and going their way to include light theming themselves).
- sony introduced playstation 2 and its accesories with full black color (only introduced limited white variant later). before that, many illegal makers deliberately made white variant of PS2 to get attention of buyers. also sometime later we see PS5 devices are promoted back with white variant.

<img width="200" src=/assets/ad0.png>
<img width="300" src=/assets/ad1.png>
<img width="300" src=/assets/ad2.jpg>

## Screenshots

<img width="300" src=/assets/a.png>
<img width="300" src=/assets/f.png>
<img width="300" src=/assets/g.png>
<img width="300" src=/assets/d.png>
<img width="300" src=/assets/b.png>
<img width="300" src=/assets/c.png>

## Credits

- gnome shell classic light theme port
https://gitlab.gnome.org/eeshugerman/gnome-shell-theme-default-light
- libadwaita theme port for legacy applications
https://github.com/lassekongo83/adw-gtk3
- gnome 41 wallpaper
https://github.com/GNOME/gnome-backgrounds/blob/gnome-41/backgrounds/adwaita-morning.png
- there also community effort porting light theme for ubuntu yaru
https://github.com/Muqtxdir/yaru-remix
- google images

optional extension (installable from gnome shell extension website)
- rounded windows corners by yilozt (rounded window corner for legacy apps)
- dash to panel by jderose9 (win 10 panel style)
- floating panel by aylur (win 11 panel style/floating)
- just perfection by just-perfection (customize > panel position > bottom)
- app icons taskbar by aztaskbar (panel taskbar)
- dash to dock by micxgx (configurable into ubuntu style dock)
- blur my shell by autnex (optional blur)

